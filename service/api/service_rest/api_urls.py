from django.urls import path
from .api_views import (
    list_technician,
    technician_delete,
    appointment_list,
    appointment_delete,
    appointment_cancel,
    appointment_finish
    )


urlpatterns = [
    path("technicians/", list_technician, name="list_technician"),
    path('technicians/<str:id>/', technician_delete, name='technician_delete'),
    path("appointments/", appointment_list, name="appointment_list"),
    path('appointments/<int:id>',
         appointment_delete,
         name='appointment_delete'),
    path('appointments/<int:id>/cancel',
         appointment_cancel,
         name='appointment_cancel'),
    path('appointments/<int:id>/finish',
         appointment_finish,
         name='appointment_finish'),
]
