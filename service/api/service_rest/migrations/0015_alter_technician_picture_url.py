# Generated by Django 4.0.3 on 2023-06-12 20:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0014_technician_picture_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='technician',
            name='picture_url',
            field=models.URLField(default='https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg'),
        ),
    ]
