from django.db import models
from django.urls import reverse

# Create your models here.


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField(null=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False, null=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=20, unique=True)
    picture_url = models.URLField(default="https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg")

    def get_api_url(self):
        return reverse("list_technician", kwargs={"id": self.employee_id})


class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    vin = models.CharField(max_length=17, null=True, blank=True)
    reason = models.TextField()
    vip_status = models.BooleanField(default=False)
    status = models.CharField(max_length=100, default="created")
    customer = models.CharField(max_length=150)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT
      )
