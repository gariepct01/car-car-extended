from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Appointment, AutomobileVO, Technician
from .encoders import TechnicianEncoder, AppointmentEncoder, AutomobileVOEncoder
# Create your views here.


@require_http_methods(["GET"])
def list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["POST"])
def technician_create(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "failed to create technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def technician_delete(request, id):
    if request.method == "DELETE":
        try:
            technicians = Technician.objects.get(employee_id=id)
            technicians.delete()
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def appointment_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )


@require_http_methods(["POST"])
def appointment_create(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:

            employee_id = content['technician']
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician

            try:
                automobile = AutomobileVO.objects.get(vin=content["vin"])
                if automobile:
                    content["vip_status"] = True
            except AutomobileVO.DoesNotExist:
                content["vip_status"] = False

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile"},
                status=400,
            )

        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def appointment_delete(request, pk):
    if request.method == "DELETE":
        appointment = Appointment.objects.get(id=pk)
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["PUT"])
def appointment_cancel(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Cancelled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def appointment_finish(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
