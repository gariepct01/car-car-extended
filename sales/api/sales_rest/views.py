from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Customer, Sale, Salesperson


class AutomobileVOEncoer(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "picture_url",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin}


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    """
    Lists the names and information of all salespersons.

    Returns a dictionary with a single key called 'salespeople' which contains
    the list of salespersons inside of it.  Each entry in this dictionary
    contains all the information of each employee.  Note that when referencing
    an employee their "employee_id" is used and not the "id".  When called as
    a "POST" it will instead create a new employee with its own template.

    "GET"
    {
        "salespeople": [
            {
                "href": href,
                "first_name": First name,
                "last_name": Last name,
                "employee_id": Employee Id (Unique)
            },
            ...
        ]
    }

    "POST"
    {
        "first_name": First name,
        "last_name": Last name,
        "employee_id": Employee Id (Unique)
    }
    """
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Exception:
            return JsonResponse(
                {"message": "Could not create salesperson, make sure the " +
                 "employee_id is not taken."},
                status=400
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_salesperson(request, id):
    """
    "GET"
    Returns a single salesperson by the ID used to call the 'get' method.

    "PUT"
    Updates a single salesperson by the ID used to call the 'put' method.

    "DELETE"
    Deletes a salesperson, where the url contains the ID of the salesperson to
    be deleted, making it safe to use when referencing or deleting salespersons
    as none will share the same id.
    """
    try:
        salesperson = Salesperson.objects.get(employee_id=id)
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid employee ID"},
            status=404,
            )

    if request.method == "GET":
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            if "employee_id" in content:
                return JsonResponse(
                    {"message": "The employee_id cannot be changed, please remove this field"},
                    status=400
                    )
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid information, please try again."},
                status=400
                )
        Salesperson.objects.filter(employee_id=id).update(**content)
        salesperson = Salesperson.objects.get(employee_id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        salesperson.delete()
        return JsonResponse(
            {"deleted": "This salesperson has been deleted"}
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    """
    Lists the names and information of all customers.

    Returns a dictionary with a single key called 'customers' which contains
    the list of customers inside of it.  Each entry in this dictionary
    contains all the information of each customer.  When called as a "POST" it
    will instead create a new customer with its own template.

    "GET"
    {
        "customers": [
            {
                "href": href
                "first_name": First name
                "last_name": Last name
                "address": Address
                "phone_number": Phone number
                "id": Customer's ID
            },
            ...
        ]
    }

    "POST"
    {
        "first_name": First name
        "last_name": Last name
        "address": Address
        "phone_number": Phone number (Validated to contain only
            numbers, spaces, or -'s)
    }
    """
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            # Tests the phone number to see if it contains only numbers,
            # spaces, or -'s
            # If the phone number contains letters or other characters,
            # returns an exception
            test_number = (content["phone_number"]
                           .replace('-', '')
                           .replace(" ", ""))
            if test_number.isnumeric() is False:
                return JsonResponse(
                    {"message": "Invalid phone number; must contain only " +
                     "numbers, spaces, or -'s"},
                    status=400
                )
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create customer"},
                status=400
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer(request, id):
    """
    "DELETE"
    Deletes a customer, where the url contains the ID of the customer to be
    deleted, making it safe to use when referencing or deleting customers as
    none will share the same id.
    """
    try:
        customer = Customer.objects.get(id=id)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer ID"},
            status=404,
        )

    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        customer.delete()
        return JsonResponse(
            {"deleted": "This customer has been deleted"}
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    """
    Lists the id's and information of all sales, including the associated
    salesperson's and customer's information.

    Returns a dictionary with a single key called 'sales' which contains
    the list of sales inside of it.  Each entry in this dictionary
    contains all the information of each sale.  When called as a "POST" it
    will instead create a new sale with its own template.

    "GET"
    {
        "sales": [
            {
                "href": href,
                "price": Price of the autommobile,
                "id": Id of the sale,
                "automobile": VIN of the automobile,
                "salesperson": {
                    "href": href,
                    "first_name": First name,
                    "last_name": Last name,
                    "employee_id": Employee Id (Unique)
                },
                "customer": {
                    "href": href
                    "first_name": First name
                    "last_name": Last name
                    "address": Address
                    "phone_number": Phone number
                    "id": Customer's ID
                }
            },
            ...
        ]
    }

    "POST"
    {
        "price": Price of the automobile
        "automobile": The automobile's VIN
        "salesperson": The employee_id of the associated salesperson
        "customer": The id of the associated customer
    }
    """
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        # Validates the price and each foreign key is legitimate and
        # creates a new sale
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid automobile VIN"},
                    status=404,
                )
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            if automobile.sold is True:
                raise Exception
        except Exception:
            return JsonResponse(
                    {"message": "This vehicle is already sold"},
                    status=400,
                )

        try:
            salesperson = Salesperson.objects.get(
                employee_id=content["salesperson"]
                )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson ID"},
                status=404,
            )

        try:
            customer = Customer.objects.get(id=content["customer"])
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=404,
            )
        content["automobile"] = automobile
        content["salesperson"] = salesperson
        content["customer"] = customer

        try:
            # Checks if the price is a number of any kind
            # Returns a TypeError if False
            if (
                isinstance(content["price"], float) is False
                and isinstance(content["price"], int) is False
            ):
                raise TypeError
            else:
                sale = Sale.objects.create(**content)
                return JsonResponse(
                    sale,
                    encoder=SaleEncoder,
                    safe=False,
                )
        except TypeError:
            return JsonResponse(
                {"message": "Price must be a number with two decimal places"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sale(request, id):
    """
    Deletes a sale, where the url contains the ID of the sale to be
    deleted, making it safe to use when referencing or deleting sales as
    none will share the same id.
    """
    try:
        sale = Sale.objects.get(id=id)
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid sale ID"},
            status=404,
        )

    if request.method == "GET":
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            if "automobile" in content:
                return JsonResponse(
                    {"message": "The automobile cannot be changed, please remove this field"},
                    status=400
                    )
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid information, please try again."},
                status=400
                )
        try:
            content["salesperson"] = Salesperson.objects.get(employee_id=content["salesperson"])
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee_id"},
                status=404,
                )
        try:
            Customer.objects.get(id=content["customer"])
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=404,
                )
        Sale.objects.filter(id=id).update(**content)
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        sale.delete()
        return JsonResponse(
            {"deleted": "This sale has been deleted"}
        )
