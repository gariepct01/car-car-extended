from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    """
    Represents an Automobile model's vin number and if it is sold or not.

    AutomobileVO is a value object and does not have its own href as a result,
    however the 'vin' can be used to identify which vehicle it is.
    """
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False, null=False)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

    def __str__(self):
        return f"{self.vin}"


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20, unique=True)
    picture_url = models.URLField(null=True)

    def get_api_url(self):
        return reverse("api_salesperson",
                       kwargs={"id": self.employee_id})

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=15)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"id": self.id})

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Sale(models.Model):
    price = models.DecimalField(max_digits=8, decimal_places=2)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_sale", kwargs={"id": self.id})
