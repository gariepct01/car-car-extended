import React, { useEffect, useState } from 'react';

function AppointmentsForm() {
    const [technicians, setTechnicians]= useState([])


    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value)
    }

    const [customerName, setCustomerName] = useState('');
    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value)
    }

    const [dateTime, setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value)
    }

    const [technician, setTechnician] = useState('');
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value)
    }

    const [reason, setReason] = useState('');
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value)
    }

    useEffect(() => {
        async function getTechs() {
            const url = "http://localhost:8080/api/technicians/";
            const response = await fetch(url)
            if(response.ok) {
                const data = await response.json()
                setTechnicians(data.technicians)
            }
        }
        getTechs();
    }, [])




    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customerName;
        data.date_time = dateTime;
        data.technician = technician;
        data.reason = reason;

        const appointmentsUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log("this is the new app", newAppointment)


            setCustomerName('');
            setVin('');
            setDateTime('');
            setReason('');
            setTechnician('');

        }
    }

    const fetchTechnicianData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);

        if (technicianResponse.ok) {
            const data = await technicianResponse.json();
            setTechnician(data.technician)
        }
    }

    useEffect(() => {
        fetchTechnicianData();
    }, []);



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div>Automobile VIN</div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin"
                                id="vin" className="form-control" value={vin} />
                        </div>
                        <div>Customer</div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerNameChange} placeholder="customerName" required type="text" name="customerName"
                                id="customerName" className="form-control" value={customerName} />
                        </div>
                        <div>Date & Time</div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange} value={dateTime} placeholder="YYYY-MM-DD HH:MM" required type="dateTime" name="dateTime" id="dateTime" className="form-control" />
                            <label htmlFor="dateTime">YYYY-MM-DD HH:MM</label>
                        </div>
                        <div>Technician</div>
                        <div className="mb-3">
                            <select onChange={handleTechnicianChange} required value={technician} className="form-select">
                                <option value="">Choose a Technician...</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div>Reason</div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="reason" type="text" name="reason" id="reason"
                                className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};








export default AppointmentsForm;
