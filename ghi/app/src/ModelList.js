import React, { useEffect, useState } from 'react'

function ModelList() {
    const [models, setModels] = useState([]);

    const handleImage = (event) => {
        event.target.src = 'https://i1.sndcdn.com/avatars-000225352686-ngevdh-t500x500.jpg';
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <h1>Models</h1>
            <div className="row">
            {models.map(model => {
                return (
                    <div className="card" key={model.id} style={{width: 300, margin: 20}}>
                        <img className="card-img-top" src={model.picture_url} alt="Model picture" style={{height: 200, width: 280}} onError={handleImage} />
                        <div className="card-body">
                            <h5 className="card-title" style={{textAlign: 'center', fontSize: 22}}>{ model.name }</h5>
                            <h6 className="card-subtitle mb-2 text-muted" style={{textAlign: 'center'}}>{ model.manufacturer.name }</h6>
                        </div>
                    </div>
                );
            })}
            </div>
        </>
    );
}

export default ModelList;
