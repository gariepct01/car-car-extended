import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelForm from './ModelForm';
import ModelList from './ModelList';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalesHistory from './SalesHistory';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import Salesperson from './Salesperson';
import TechForm from './TechForm';
import TechList from './TechList';
import AppointmentsForm from './AppointmentsForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import AutomobileAvailability from './AutomobilesAvailable';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
            <Route path="available" element={<AutomobileAvailability />} />
            <Route path="new" element={<AutomobileForm />}/>
          </Route>
          <Route path="models">
            <Route path="" element={<ModelList />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalespersonList />} />
            <Route path="create" element={<SalespersonForm />} />
            <Route path=":id" element={<Salesperson />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechForm />} />
            <Route path="" element={<TechList />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentsForm />} />
            <Route path="" element={<AppointmentList />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
