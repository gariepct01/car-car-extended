import React, { useState, useEffect } from 'react';

function TechForm() {
    const [firstName, setFirstName] = useState('')
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value)
    }

    const [lastName, setLastName] = useState('')
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const [employeeID, setEmployeeID] = useState('')
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value)
    }

    const [pictureUrl, setPictureUrl] = useState('');
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const [technicians, setTechnicians] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        let invalidID = false;
        for (const techie of technicians) {
            if (techie.employee_id === employeeID) {
                invalidID = true;
            }
        }


        if (invalidID === true) {
            setEmployeeID('');
            alert("Error: EmployeeID is already in use, please try a different one.");
        } else {
            const data = {};

            data.first_name = firstName;
            data.last_name = lastName;
            data.employee_id = employeeID;
            data.picture_url = pictureUrl;

            const technicianUrl = "http://localhost:8080/api/technicians/"
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };


            const response = await fetch(technicianUrl, fetchConfig);
            if (response.ok) {
                const newTechnician = await response.json();

                setFirstName('');
                setLastName('');
                setEmployeeID('');
                setPictureUrl('');
            }

        }}

        const fetchData = async () => {
            const url = 'http://localhost:8080/api/technicians/';
            const response = await fetch(url);

            if (response.ok) {
                const technicianData = await response.json();
                setTechnicians(technicianData.technicians);
            }
        }

        useEffect(() => {
            fetchData();
          }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} placeholder="FirstName" required type="text" name="firstName"
                                id="firstName" className="form-control" value={firstName} />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} placeholder="LastName" required type="text" name="lastName"
                                id="lastName" className="form-control" value={lastName} />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIDChange} value={employeeID} placeholder="EmployeeID" required type="text" name="employeeID"
                                id="employeeID" className="form-control" />
                            <label htmlFor="employeeID">Employee ID</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="pictureUrl" type="url" name="pictureUrl" id="pictureUrl" value={pictureUrl} className="form-control"/>
                            <label htmlFor="pictureUrl">Picture Url (Optional)</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default TechForm;
