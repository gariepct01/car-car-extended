import React, { useEffect, useState } from 'react'


function SalesForm() {
    // Called when a sale is successful, changing the automobile's status to sold
    async function AutomobileSold(vin) {
        const automobileUrl = `http://localhost:8100/api/automobiles/${vin}/`

        const data = {};
        data.sold = true;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // If response is good, sold is updated and the VIN dropdown is refreshed
        const response = await fetch(automobileUrl, fetchConfig)
        if (response.ok) {
            fetchData();
        }
    }

    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Checks if the price contains only numbers, returns an alert if not
        // If false continues to create the sale
        if (isNaN(price) === true) {
            setPrice('');
            alert("Error: Please make sure the price contains only numbers!");
        } else {
            const data = {};
            data.price = Number(price);
            data.automobile = automobile;
            data.salesperson = salesperson;
            data.customer = customer;

            const saleUrl = 'http://localhost:8090/api/sales/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(saleUrl, fetchConfig);
            if (response.ok) {
                AutomobileSold(automobile);
                setAutomobile('');
                setSalesperson('');
                setCustomer('');
                setPrice('');
            }
        }
    }


    const fetchData = async() => {
        // Delcaring three urls to fetch from and placing them into the
        // requests array, called in order of automobiles -> salespeople -> customers
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const customersUrl = 'http://localhost:8090/api/customers/';

        const requests = [];
        requests.push(fetch(automobilesUrl));
        requests.push(fetch(salespeopleUrl));
        requests.push(fetch(customersUrl));

        // Waits for all requests to finish at the same time
        const responses = await Promise.all(requests);

        // Loops over the responses array, setting each data set
        let count = 0;
        for (const response of responses) {
            if (response.ok) {
                const data = await response.json();
                if (count === 0) {
                    setAutomobiles(data.autos);
                    count = 1;
                } else if (count === 1) {
                    setSalespeople(data.salespeople);
                    count = 2;
                } else {
                    setCustomers(data.customers);
                    count = 0;
                }
            }
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <h1>Record a new sale</h1>
                        <div className="mb-3">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select onChange={handleAutomobileChange} required id="automobile" name="automobile" className="form-select" value={automobile}>
                                <option value="">Choose an automobile VIN...</option>
                                {automobiles.map(automobile => {
                                    if (automobile.sold === false) {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                { automobile.vin }
                                            </option>
                                        );
                                    }
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="salesperson">Salesperson</label>
                            <select onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select" value={salesperson}>
                                <option value="">Choose a salesperson...</option>
                                {salespeople.map(employee => {
                                    return (
                                        <option key={employee.employee_id} value={employee.employee_id}>
                                            { employee.first_name} {employee.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleCustomerChange} required id="customer" name="customer" className="form-select" value={customer}>
                                <option value="">Choose a customer...</option>
                                {customers.map(purchaser => {
                                    return (
                                        <option key={purchaser.phone_number} value={purchaser.id}>
                                            { purchaser.first_name} { purchaser.last_name }
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Choose a price..." required type="text" name="price" id="price" className="form-control" value={price} />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
