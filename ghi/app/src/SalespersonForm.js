import React, { useEffect, useState } from 'react'


function SalespersonForm() {
    const [salespeople, setSalespeople] = useState([]);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeID, setEmployeeID] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value);
    }

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value
        setEmployeeID(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Checks to make sure the employee_id is not in use
        // If it is, invalidID is marked as true
        let invalidID = false;
        for (const person of salespeople) {
            if (person.employee_id === employeeID) {
                invalidID = true;
            }
        }

        // Creates an alert if the EmployeeID is in use
        // Otherwise creates a new salesperson
        if (invalidID === true) {
            setEmployeeID('');
            alert("Error: EmployeeID is already in use, please try a different one.");
        } else {
            const data = {};
            data.first_name = firstName;
            data.last_name = lastName;
            data.employee_id = employeeID;
            data.picture_url = pictureUrl;

            const salespersonUrl = "http://localhost:8090/api/salespeople/"
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(salespersonUrl, fetchConfig);
            if (response.ok) {
                setFirstName('');
                setLastName('');
                setEmployeeID('');
                setPictureUrl('');
                fetchData();
            }
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);

        if (response.ok) {
            const salespersonData = await response.json();
            setSalespeople(salespersonData.salespeople);
        }
    }


    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} placeholder="FirstName" required type="text" name="firstName" id="firstName" className="form-control" value={firstName} />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} placeholder="LastName" required type="text" name="lastName" id="lastName" className="form-control" value={lastName} />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIDChange} placeholder="EmployeeID" required type="text" name="employeeID" id="employeeID" className="form-control" value={employeeID} />
                            <label htmlFor="employeeID">Employee ID</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="pictureUrl" type="url" name="pictureUrl" id="pictureUrl" value={pictureUrl} className="form-control"/>
                            <label htmlFor="pictureUrl">Picture Url (Optional)</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalespersonForm;
