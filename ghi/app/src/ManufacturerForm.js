import React, { useEffect, useState } from 'react'

function ManufacturerForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Checks to make sure the manufacturer is not already made
        // If it is, invalidManufacturer is marked as true
        let invalidManufacturer = false;
        for (const manufacturer of manufacturers) {
            if (manufacturer.name === name) {
                invalidManufacturer = true;
            }
        }

        // Creates an alert if the name already exists
        // Otherwise creates a new manufacturer
        if (invalidManufacturer === true) {
            setName('');
            alert("Error: Manufacturer name is already in use, please try a different one.");
        } else {
            const data = {};
            data.name = name;
            data.picture_url = pictureUrl;

            const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(manufacturerUrl, fetchConfig);
            if (response.ok) {
                setName('');
                setPictureUrl('');
                fetchData();
            }
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok) {
            const manufacturerData = await response.json();
            setManufacturers(manufacturerData.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="pictureUrl" required type="url" name="pictureUrl" id="pictureUrl" value={pictureUrl} className="form-control"/>
                            <label htmlFor="pictureUrl">Picture Url</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
