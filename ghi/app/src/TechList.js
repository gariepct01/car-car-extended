import React, { useEffect, useState } from 'react';


function TechList() {

  const [technicians, setTechnicians] = useState([]);

  const handleImage = (event) => {
    event.target.src = 'https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg';
  }

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
        <h1>Technicians</h1>
        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => {
                    return (
                        <tr key={technician.employee_id}>
                            <td>{technician.employee_id}</td>
                            <td>{technician.first_name}</td>
                            <td>{technician.last_name}</td>
                            <td><img src={technician.picture_url} style={{height: 100, width: 100}} onError={handleImage} /></td>
                        </tr>
                );
                })}
            </tbody>
        </table>
    </>
  );
}
export default TechList;
