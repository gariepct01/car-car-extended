import React, { useEffect, useState } from 'react'

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <h1>Manufacturers</h1>
            <div className="row">
                {manufacturers.map(manufacturer => {
                    return (
                        <div className="card" key={manufacturer.id} style={{width: 200, margin: 20}}>
                            <img className="card-img-top" src={manufacturer.picture_url} alt="Manufacturer logo" style={{height: 180, width: 180, padding: 5}} />
                            <div className="card-body">
                                <h5 className="card-title" style={{textAlign: 'center'}}>{ manufacturer.name }</h5>
                            </div>
                        </div>
                    );
                })}
            </div>
        </>
    );
}

export default ManufacturerList;
