import {useState, useEffect} from 'react';

function AutomobileAvailability() {
    const [manufacturers, setManufacturers] = useState([]);
    const [models, setModels] = useState([]);
    const [autos, setAutomobiles] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');

    const handleImage = (event) => {
        event.target.src = 'https://www.shutterstock.com/image-vector/car-logo-icon-emblem-design-260nw-473088025.jpg';
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
        setModel('');
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const fetchData = async() => {
        const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
        const modelsUrl = "http://localhost:8100/api/models/";
        const automobilesUrl = "http://localhost:8100/api/automobiles/";

        const requests = [];
        requests.push(fetch(manufacturersUrl));
        requests.push(fetch(modelsUrl));
        requests.push(fetch(automobilesUrl));

        // Waits for all requests to finish at the same time
        const responses = await Promise.all(requests);

        // Loops over the responses array, setting each data set
        let count = 0;
        for (const response of responses) {
            if (response.ok) {
                const data = await response.json();
                if (count === 0) {
                    setManufacturers(data.manufacturers);
                    count = 1;
                } else if (count === 1) {
                    setModels(data.models);
                    count = 2;
                } else {
                    setAutomobiles(data.autos);
                    count = 0;
                }
            }
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <h1>Available Automobiles</h1>
            <select onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" className="form-select" value={manufacturer}>
            <option value="">Select a manufacturer</option>
                {manufacturers.map(manufacturer => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.name}>
                            { manufacturer.name }
                        </option>
                    );
                })}
            </select>
            <select onChange={handleModelChange} required id="model" name="model" className="form-select" value={model}>
            <option value="">Select a model</option>
                {models.map(model => {
                    if (model.manufacturer.name === manufacturer) {
                        return (
                            <option key={model.id} value={model.name}>
                                { model.name }
                            </option>
                        );
                    }
                })}
            </select>
            <div className="row">
                {autos.map(auto => {
                    if (auto.model.manufacturer.name === manufacturer && auto.sold === false) {
                        if (auto.model.name === model) {
                            return (
                                <div className="card" key={auto.id} style={{width: 300, margin: 20}}>
                                    <h5 className="card-title" style={{fontSize: 22}}>VIN: { auto.vin }</h5>
                                    <img className="card-img-top" src={auto.picture_url} alt="Model picture" style={{height: 200, width: 280}} onError={handleImage} />
                                    <div className="card-body">
                                        <h5 className="card-title" style={{textAlign: 'center', fontSize: 22}}>{ auto.model.manufacturer.name } - { auto.model.name }</h5>
                                        <p className="card-text" style={{textAlign: 'center'}}>Year: { auto.year }</p>
                                        <p className="card-text" style={{textAlign: 'center'}}>Color: { auto.color }</p>
                                    </div>
                                </div>
                            );
                        } else if (model === '') {
                            return (
                                <div className="card" key={auto.id} style={{width: 300, margin: 20}}>
                                    <h5 className="card-title" style={{fontSize: 22}}>VIN: { auto.vin }</h5>
                                    <img className="card-img-top" src={auto.picture_url} alt="Model picture" style={{height: 200, width: 280}} onError={handleImage} />
                                    <div className="card-body">
                                        <h5 className="card-title" style={{textAlign: 'center', fontSize: 22}}>{ auto.model.manufacturer.name } - { auto.model.name }</h5>
                                        <p className="card-text" style={{textAlign: 'center'}}>Year: { auto.year }</p>
                                        <p className="card-text" style={{textAlign: 'center'}}>Color: { auto.color }</p>
                                    </div>
                                </div>
                            );
                        }
                    }
                })}
            </div>
        </>
    );
}

export default AutomobileAvailability;
