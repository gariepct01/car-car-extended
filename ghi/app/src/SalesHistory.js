import React, { useEffect, useState } from 'react'


function SalesHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');

    const handleSalesperson = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const fetchData = async () => {
        // Declares two Urls to fetch from and placing them into the
        // requests array, called in order of sales -> salespeople
        const salesUrl = 'http://localhost:8090/api/sales/';
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';

        const requests = [];
        requests.push(fetch(salesUrl));
        requests.push(fetch(salespeopleUrl));

        // Waits for all requests to finish at the same time
        const responses = await Promise.all(requests);

        // Loops over the responses array, setting each data set
        let count = 0;
        for (const response of responses) {
            if (response.ok) {
                const data = await response.json();
                if (count === 0) {
                    setSales(data.sales);
                    count = 1;
                } else {
                    setSalespeople(data.salespeople);
                    count = 0;
                }
            }
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <h1>Salesperson History</h1>
            <select onChange={handleSalesperson} required id="salesperson" name="salesperson" className="form-select" value={salesperson}>
            <option value="">Select a salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            { salesperson.first_name } { salesperson.last_name }
                        </option>
                    );
                })}
            </select>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        if (sale.salesperson.employee_id === salesperson) {
                            return (
                                <tr key={ sale.id + "-" + sale.automobile }>
                                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                    <td>{ sale.automobile }</td>
                                    <td>${ sale.price }</td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SalesHistory;
