import { useLocation, Link } from "react-router-dom";
import React, { useEffect, useState } from 'react'
import './profile.css'


function Salesperson() {
    const [employee, setEmployee] = useState([]);
    const salesperson = useLocation();

    const handleImage = (event) => {
        event.target.src = 'https://st3.depositphotos.com/6672868/13701/v/450/depositphotos_137014128-stock-illustration-user-profile-icon.jpg';
    }

    const fetchData = async () => {
        const url = `http://localhost:8090/api${salesperson.pathname}`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setEmployee(data);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-sm profile-shadow p-4 mt-4">
                        <h1>{employee.first_name} {employee.last_name}</h1>
                        <h2>ID: { employee.employee_id }</h2>
                        <img src={employee.picture_url} style={{height: 200, width: 200}} onError={handleImage} />
                    </div>
                    <div className="col-sm profile-shadow p-4 mt-4">
                    </div>
                    <Link to="/salespeople">
                        <h1><button className="btn btn-primary">Return to salespeople</button></h1>
                    </Link>
                </div>
            </div>
        </>
    );
}

export default Salesperson;
