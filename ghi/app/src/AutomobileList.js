import {useState, useEffect} from 'react';

function CreatePage(props) {
    const handleImage = (event) => {
        event.target.src = 'https://www.shutterstock.com/image-vector/car-logo-icon-emblem-design-260nw-473088025.jpg';
    }
    return (
        <div className="card" key={props.auto.id} style={{width: 310, margin: 20, textAlign: 'center'}}>
            <h5 className="card-title" style={{fontSize: 22}}>VIN: { props.auto.vin }</h5>
            <img className="card-img-top" src={props.auto.picture_url} alt="Model picture" style={{height: 200, width: 280}} onError={handleImage} />
            <div className="card-body">
                <h5 className="card-title" style={{textAlign: 'center', fontSize: 22}}>{ props.auto.model.manufacturer.name } - { props.auto.model.name }</h5>
                <p className="card-text" style={{textAlign: 'center'}}>Year: { props.auto.year }</p>
                <p className="card-text" style={{textAlign: 'center'}}>Color: { props.auto.color }</p>
                <p className="card-text" style={{textAlign: 'center'}}>Sold: {props.auto.sold === true ? "yes" : "no"}</p>
            </div>
        </div>
    );
}

function AutomobileList () {
    const [autos, setAutos] = useState([]);
    const [sold, setSold] = useState('');

    const handleSold = (event) => {
        const value = event.target.value;
        setSold(value);
    }

    const fetchData = async() => {
        const Url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(Url);

            if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
            setSold("0");
            }
    }

    useEffect(() => {fetchData();
    }, []);

return (
    <>
        <h1>Automobiles</h1>
        <div className="btn-group btn-group-sm" role="group" aria-label="Basic example">
            <button type="button" className="btn btn-secondary" onClick={handleSold} value="0">All</button>
            <button type="button" className="btn btn-secondary" onClick={handleSold} value="1">Sold</button>
            <button type="button" className="btn btn-secondary" onClick={handleSold} value="2">Unsold</button>
        </div>
            <div className="row">
                {autos.map(auto => {
                    if (sold === "0") {
                        return (
                            <CreatePage key={auto.id} auto={auto} />
                        );
                    } else if (sold === "1" && auto.sold === true) {
                        return (
                            <CreatePage key={auto.id} auto={auto} />
                        );
                    } else if (sold === "2" && auto.sold === false) {
                        return (
                            <CreatePage key={auto.id} auto={auto} />
                        );
                    }
                })}
            </div>
    </>
    );
}

export default AutomobileList;
