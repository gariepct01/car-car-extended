import React, { useEffect, useState } from "react";

function AutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [model, setModel] = useState('')
    const [automobiles, setAutomobiles] = useState([]);
    const [models, setModels] = useState([])

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Checks to make sure the vin is not already made
        // If it is, invalidVin is marked as true
        let invalidVin = false;
        for (const automobile of automobiles) {
            if (automobile.vin === vin) {
                invalidVin = true;
            }
        }

        // Creates an alert if the vin already exists
        // Otherwise creates a new automobile
        if (invalidVin === true) {
            setVin('');
            alert("Error: VIN is already in use, please try a different one.");
        } else {
            const data = {};

            data.color = color;
            data.year = year;
            data.picture_url = pictureUrl;
            data.vin = vin;
            data.model_id = model;

            const automobileUrl = "http://localhost:8100/api/automobiles/"
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(automobileUrl, fetchConfig)
            if(response.ok) {
                setColor('');
                setYear('');
                setVin('');
                setPictureUrl('');
                setModel('');
                fetchData();
            }
        }
    }

    const fetchData = async () => {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const modelUrl = "http://localhost:8100/api/models/";

        const requests = [];
        requests.push(fetch(automobileUrl));
        requests.push(fetch(modelUrl));

        // Waits for all requests to finish at the same time
        const responses = await Promise.all(requests);

        // Loops over the responses array, setting each data set
        let count = 0;
        for (const response of responses) {
            if (response.ok) {
                const data = await response.json();
                if (count === 0) {
                    setAutomobiles(data.autos);
                    count = 1;
                } else {
                    setModels(data.models);
                    count = 0;
                }
            } else {
                console.error(response);
            }
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2 className="display-5 text-center"><b>Add an automobile to inventory</b></h2>
                    <form onSubmit={handleSubmit} id="create-automobile">
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" value={color} className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleYearChange} placeholder="year" required type="text" name="year" id="year" value={year} className="form-control"/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" value={vin} className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                        <select onChange={handleModelChange} value={model} required name="model" id="model" className="form-select">
                        <option value="">Choose a model</option>
                            {models.map(model => {
                                return (
                                    <option value={model.id} key={model.id}>
                                        {model.manufacturer.name} - {model.name}
                                    </option>
                                )
                            })}
                        </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="pictureUrl" type="url" name="pictureUrl" id="pictureUrl" value={pictureUrl} className="form-control"/>
                            <label htmlFor="pictureUrl">Picture Url (Optional)</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default AutomobileForm;
