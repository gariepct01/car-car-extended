import React, { useEffect, useState } from "react";

function ServiceHistory (){
   const[appointments, setAppointments] = useState([])
   const[vinQuery, setVinQuery] = useState("");
   const[vinResult, setVinResult] = useState(appointments);

   const fetchData = async() => {
        const Url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(Url);

            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
                setVinResult(data.appointments);
            }
    }

    useEffect(() => {fetchData();
    }, []);

   const handleVinQueryChange = (event) => {
    setVinQuery(event.target.value);
   };

   const handleSearchButtonChange = () => {
    if (vinQuery.trim() !== "") {
      const query = appointments.filter((appointment) => {
        return vinQuery === appointment.vin;
      });
      setVinResult(query);
    } else {
      setVinResult(appointments);
    }
  };



    return (
    <div className="container">
      <h2 className="my-4">Service History</h2>
      <div className="form-group">
      <label htmlFor="vin-search">Search by VIN:</label>
      <input type="text" className="form-control" id="vin-search" value={vinQuery} onChange={handleVinQueryChange} />
        <button onClick={handleSearchButtonChange}
        type="button"
        className="btn btn-primary">
        Search
        </button>
      </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>is VIP?</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {vinResult.map(appointment => {
                return(
                <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.customer}</td>
                    <td>{appointment.vip_status === true ? "yes" : "no"}</td>
                    <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                    <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status}</td>
                </tr>
                );

            })}
            </tbody>
        </table>
    </div>
)
}
export default ServiceHistory
