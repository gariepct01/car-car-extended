# CarCar

Team:

* Person 1 - Which microservice?
Connor Gariepy - Automobile Sales
Demi Dosunmu - Automobile Service
## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.
I'll first start by creating a model for the automobileVO, Technician, and Appointment. Once the models are created, I'll want to get the ploller up and workign to ensure that data can be pulled from Automobile via the Automobile VO.

After that, I'll start creating view functions, create some entries via Insomnia, and ensure that PUT, GET, DELETE,PUSH, POST requests are workign as intended.

When the back end is set up correctly, we can start to use React to build out the front end.

## Sales microservice

To start with my microservice I'm first going to make the models for
the Salesperson, Customer, Sale, and AutomobileVO, and once those have been
migrated and work I will then get the poller working to ensure that the
AutomobileVO can be populated.

After I've done that I will start creating the requested views and making
sure each error code works for them.

Once the views have been created I will start on the front-end and begin
working through each of the pages and implenting each one before moving
onto the Inventory shared portion of the project with Demi.
